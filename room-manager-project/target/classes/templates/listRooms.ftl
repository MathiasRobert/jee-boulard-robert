<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
    <h1>${title}</h1>
    
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Taux de remplissage</th>
            </tr>
        </thead>
        <tbody>
        <#list rooms as room>
            <tr>
                <td>${room.id}</td>
                <td>${room.name}</td>
            <#if room.people gt room.capacity*0.70>
                <td style="background-color:red;">
                <#elseif room.people gt room.capacity*0.40>
                    <td style="background-color:orange;">
                    <#else>
                        <td style="background-color:green;">
                        
            </#if>
                ${room.people}/${room.capacity}</td>
                <td>
                    <a type="button" class="btn btn-success" href="entreeSortie?name=${room.name}&type=1">
                      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                </td>
                <td>
                    <a type="button" class="btn btn-danger" href="entreeSortie?name=${room.name}&type=-1">
                       <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                    </a>
                </td>
            <tr>
        </#list>
        </tbody>
    </table>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>