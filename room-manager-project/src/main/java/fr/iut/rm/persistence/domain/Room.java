package fr.iut.rm.persistence.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A classic room
 */
@Entity
@Table(name = "room")
public class Room {
    /**
     * sequence generated id
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * Room's name
     */
    @Column(nullable = false, unique = true)
    private String name;

    /**
     * Room's capacity 
     */
    @NotNull
    @Min(0)
    private int capacity;

    /**
     * Room's people 
     */
    @Min(0)
    private int people;
    
    /**
     * Default constructor (do nothing)
     */
    public Room() {
        // do nothing
    }

    /**
     * anemic getter
     *
     * @return the room's id
     */
    public long getId() {
        return id;
    }

    /**
     * anemic setter
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * anemic getter
     *
     * @return the calling number
     */
    public String getName() {
        return name;
    }

    /**
     * anemic setter
     *
     * @param name the new calling number to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * anemic getter
     *
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * anemic setter
     *
     * @param capacity the new capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * anemic getter
     *
     * @return the people
     */
    public int getPeople() {
        return people;
    }

    /**
     * anemic setter
     *
     * @param people the new people to set
     */
    public void setPeople(int people) {
        this.people = people;
    }
}
