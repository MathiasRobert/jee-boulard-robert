package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;

/**
 * This servlet simply
 */
@Singleton
public class EntreeSortieServlet extends HttpServlet {
    /**
     * the dao used to access room persisted data
     */
    @Inject
    RoomDao roomDao;

    /**
     * HTTP GET access
     * @param req use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws ServletException by container
     * @throws IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        int type = 0;
        String name = "";
        String param = req.getParameter("name");
        String param2 = req.getParameter("type");
        if (param != null) {
            name = param;
        }
        if (param2 != null) {
            type = Integer.parseInt(param2);
        }

        Room r = roomDao.findByName(name);
        if (r.getPeople() + type <= r.getCapacity()) {
            try {
                r.setPeople(r.getPeople() + type);
            } catch (ConstraintViolationException e) {
                e.getMessage();
            }
        }
        roomDao.saveOrUpdate(r);
        
        //resp.sendRedirect(req.getRequestURL().toString());
        resp.sendRedirect("home");
    }
}
